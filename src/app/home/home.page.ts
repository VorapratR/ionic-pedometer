import { Component, NgZone } from '@angular/core';
import { Pedometer, IPedometerData } from '@ionic-native/pedometer/ngx';
import { ToastController, Platform } from '@ionic/angular';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage {
  start: boolean;
  pedometerData: any;
  stepCount: any = 0;
  distance: any = 0;
  constructor(
    public toastCtrl: ToastController,
    private ngZoneCtrl: NgZone,
    public platformCtrl: Platform,
    public pedoCtrl: Pedometer
  ) {
    this.stepCount = 0;
    this.distance = 0;
  }

  fnGetPedoUpdate() {
    if (this.platformCtrl.is('cordova')) {
      this.pedoCtrl.startPedometerUpdates().subscribe(PedometerData => {
        this.pedometerData = PedometerData;
        this.ngZoneCtrl.run(() => {
          this.stepCount = this.pedometerData.distance;
          this.distance = this.pedometerData.numberOfSteps;
        });
      });
      this.start = true;
      this.fnTost('Please Walk🚶‍to Get Pedometer Update.');
    } else {
      this.fnTost('This application needs to be run on📱device');
    }
  }

  fnStopPedoUpdate() {
    this.pedoCtrl.stopPedometerUpdates();
    this.fnTost('This application needs to be run on📱device');
    this.start = false;
  }

  async fnTost(message) {
    const toast = this.toastCtrl.create({
      message,
      position: 'bottom',
      duration: 3000
    });
    (await toast).present();
  }
}
